import PySimpleGUI as gui
from pytube import YouTube

gui.theme('SystemDefault')
program_name = "Youtube Downloader"

start_window = [[gui.Input(expand_x=True,key='-INPUT-'), gui.Button('Submit')]]


info_tab = [[gui.Text('Title: '), gui.Text('', key='-TITLE-')],
            [gui.Text('Length: '), gui.Text('', key='-LENGTH-')],
            [gui.Text('Views: '), gui.Text('', key='-VIEWS-')],
            [gui.Text('Aothur: '), gui.Text('', key='-AUTHOR-')],
            [
                gui.Text('Description: '),
                gui.Multiline('', key='-DESCRIPTION-', size=(40, 20),
                              no_scrollbar=True, disabled=True)
]]

download_tab = [
    [gui.Frame('Best Quality', [[gui.Button('Download', key='-BEST-'),
               gui.Text('', key='-BESTRES-'), gui.Text('', key='-BESTSIZE-')]])],
    [gui.Frame('Worst Quality', [[gui.Button('Download', key='-WORST-'),
               gui.Text('', key='-WORSTRES-'), gui.Text('', key='-WORSTSIZE-')]])],
    [gui.Frame('Audio', [[gui.Button('Download', key='-AUDIO-'),
               gui.Text('', key='-AUDIOSIZE-')]])],
    [gui.VPush()],
    [gui.Progress(100, orientation='h', size=(20, 20),
                  key='-DOWNLOADPROGRESS-', expand_x=True)]
]
layout = [
    [gui.TabGroup([[gui.Tab('Info', info_tab), gui.Tab('Download', download_tab)]])]]

window = gui.Window(program_name, start_window)


video_link = 'https://www.youtube.com/watch?v=rPSbJDxvt8A'

while True:
    event, Values = window.read()

    if event == gui.WINDOW_CLOSED:
        break
    
    if event == 'Submit':
        video_obeject = YouTube(Values['-INPUT-'])
        window.close()
        window = gui.Window(program_name,layout,finalize=True)
        window['-TITLE-'].update(video_obeject.title)
        window['-LENGTH-'].update(f'{round(video_obeject.length/60,2)} min')
        window['-VIEWS-'].update(video_obeject.views)
        window['-AUTHOR-'].update(video_obeject.author)
        window['-DESCRIPTION-'].update(video_obeject.description)
        window['-BESTSIZE-'].update(f'{round(video_obeject.streams.get_highest_resolution().filesize / 1048576,1)} MB')
        window['-BESTRES-'].update(video_obeject.streams.get_highest_resolution().resolution)
        window['-WORSTSIZE-'].update(f'{round(video_obeject.streams.get_lowest_resolution().filesize / 1048576,1)} MB')
        window['-WORSTRES-'].update(video_obeject.streams.get_lowest_resolution().resolution)
        window['-AUDIOSIZE-'].update(f'{round(video_obeject.streams.get_audio_only().filesize / 1048576,1)} MB')


    if event == '-BEST-':
        video_obeject.streams.get_highest_resolution().download()
    if event == '-WORST-':
        video_obeject.streams.get_lowest_resolution().download()	
    if event == '-AUDIO-':
        video_obeject.streams.get_audio_only().download()
    

window.close()
